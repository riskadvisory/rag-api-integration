import logging
from datetime import datetime
from datetime import timedelta

import jwt
import requests
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

from settings import API_KEY
from settings import JWT_AUDIENCE
from settings import JWT_AUTHORIZED_PARTY
from settings import JWT_ISSUER
from settings import RAG_HOST

logger = logging.getLogger(__name__)

PEM_BYTES = b'''-----BEGIN RSA PRIVATE KEY-----
MIIG5AIBAAKCAYEArmbd9f1uxgIKVwBy2eVtHE1YNC+lMh8oFZTbOMMKkBC0uaNB
y5VwdW8++OeTtJl9DEFMWSsCTOQbXx6EKWfGWLlT+lUD5K1/zvzHvEpVVgRCc5mf
FFg8+Jnb7lTA5om3YXdNtOzHBKr3H80JG2fZrU9gw4ZOZlm0yLON5gzLd0sST/JJ
/VtKSNcMsmVfdqHG/nDMz7t0S15f7l8jmuLTVbmF+H9qRVqYlWorA5UokLBTtnXf
/ne3f+tT0Biu1/aaPnjN8D1FqXB+M4Hnyz39owGyaMeyy/oH2N5TSt7vJO8IKXYQ
yArGMiyOki/Q7zWDxhSCQFL9EXmUPKNsMUmvxzevOBYQGq9VvkY9x+qBOe8E93M1
1v2t57VI4XxWDpZEjMGprNOf/57n/xAf11nyH43bCbowIeLtG0G6lLezAPKBkTq/
GMQWZaVVibhRVKFxjcg3SsIM/sVo1WmCXKD/HUqE8Ji3FlO0u5jGuQ4Je20yJR2j
9etqxnV+/70qNbvzAgMBAAECggGAbR/ifJwSJwV50MxwENVUe2QN78FsPTtviml0
7kRkJJa/bnSvo6IjFuf/4CCOtG9h5jwpeJXHv7LvSYo/qeTMIBH+cOGnKPxCv5I9
rBxa3xKVPd3+wu0fYXJsQel4Nggv3WWYEsAhG1Oag6R4sVQ+yQmlA1LTVYmF7Bsl
i6+v1Dn+AFYbY9MuXPtqb2ms3P8HE4gjL0wczVgE8JNJJQ4OrFIEeOPI7BJu7Pbc
aUZ4dmhOSRniiUgBYqIt9EStimY7PluXOVSizfY8FzH9Pl5nIdXZ+mr6oDkP6g/T
qsR3WyLQGkyFHZOnbKO9VCf1dbQDs01JzmQSLTRRmJ+K56eCfli3nSqr+avxyMgZ
FDLH+QAAIfFi3SmasPaG9X1zOY3319tD+pIvti6vL5gB0iInz/dyFi/Zw0Q3R6m4
WSHShb4a9SDmLCmLaIQEwvNUc7WuFH+OwF+wN1GGBM7EKLD3fYN3/OLIYxgoQ9nC
YIzcHLaUbTabjsSi/3+/Csdu5fApAoHBAN5s/er7tSHPJo11aRobyAyL81Kn3BkA
vE5SN92u91LPSBqdlnPkVmecxI3Qpc0W8u1MdniUMaQqFw580SABQdDpUqKLW+/F
yG4/iD5BX31ZJP7k2II4K0tILZoHD+FnSl0zBLJitAKfhKnCEnnlyaR6927uXKyx
BTvpJpzgG+4Sn6BZDxmqUP1mto+F50wYOu9oL5L2pY0OV/62GYXbSZjfBL1Wnw+2
mjXLmW+7eGKbgUOE2zblSTZq73RHGFmKlwKBwQDIuiCvSXEzE7eRL0vEUfBdnN8X
fefMBzwG5/2rYK+2V6QO5ESaLW9VbSKltB3+hRB9+ry3e8SbrEcK8lMuFbSgLblv
QLGuSxZVA9UBivC7VK+1OVA5WASbu1xqskS3NIZR35cQY89jNYejy3ynvMZvrdLO
t9/MlRHeZ0E9LiYzIJqb3H3XVeehS5RTxlq7XhIHPQGbTKu6ch4nD1kV3ixwt8D/
qATZPlwts9Er4tWoj/WQ855uDZrw+BjnEmYOEQUCgcEA3bFSsGirVARtj6cL18sC
7IZ9QtAuTvlttr66Egm3V2Fko6qYb/CRsnoNarAg4bbS8HQ6zNogVFjjXDxKEBVQ
rVIxW/N88hJ3QHOwbySncX9toWUk6z3i4QkS8qfBS0+xTg1wzAAXHVozVyQpWDgR
cBBkGLwZwMI7zkgHhWrWS8diNOcvZsxcJwW+RnUXYb/w1fhJGrkdrucyC50i+Ujl
JSp8Rd9VcMr3D1FcIIKBbf9CqrJ78E4+V6/j9Uh7W5wHAoHAeHXK26b1ejOg/E4n
6B9MYsGb2JtEmlrVGUfsIgcaLBGYcR9oVCZkJB4kptDbFz1/0jrBOlPaf5n54IXg
iU2ZYm9OU4e9md2R/18XW5+oLKP79oP4sxExYswkABwPxOqOmV/XZoyfjug3CIM7
weY2KBtm4YvTkRD88j2BF1g0sIYmgtbl5m61XUTgNQ5asQsZQqLH7g0rGe8WTVaW
ndsBTHvaJRMOJdmYtAyvwOiWVFxggKbTMzSTAX3CZBnUiSiJAoHBAK4tbUu5DDMw
iwJGkSprUVnEL+Vif4ZjCWgjc6FSyZYWtlRALai2Nee0H27tXUHCHP3RIxO0sXtO
njsnal/t60DchyqemqyJgj8qqqzuYXQgD7uP9Jzm4fAm2EwDTj3jH+reyMk6crCU
eFfX45Ww9UiZFjIGA2ipnbWUw7fPv2bvhAeI/KjWOUZLUfhYqojc7SfZHj9bPsQw
q9MhcY5UeH572TTCCdvpxnd9EZf4f7Gw7TtyP/5zTAiUN1iGXC/ttg==
-----END RSA PRIVATE KEY-----'''
PASSPHRASE = None


def get_token(token_dict):
    private_key = serialization.load_pem_private_key(
        PEM_BYTES, password=PASSPHRASE, backend=default_backend()
    )
    encoded = jwt.encode(token_dict, private_key, algorithm='RS256')

    return encoded


def get_auth_health_check():
    url = f'{RAG_HOST}/v1/auth_check'
    token_dict = {
        'iss': JWT_ISSUER,
        'azp': JWT_AUTHORIZED_PARTY,
        'aud': JWT_AUDIENCE,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(minutes=5)
    }
    params = {
        'key': API_KEY,
    }
    token = get_token(token_dict)
    print(f'Making call to {url} with params {params} and auth token {token}')
    r = requests.get(url, params=params, headers={'Authorization': f'Bearer {token}'})
    print(f'Response status code {r.status_code}')
    response_json = r.json()
    if r.status_code > 399:
        print(f'Got error back {response_json}')
    else:
        print(f'Response data {response_json["data"]}')


if __name__ == '__main__':
    get_auth_health_check()
