"""Default configuration

Use env var to override
"""
from environs import Env

env = Env()
env.read_env()

RAG_HOST = env.str('RAG_HOST')
API_KEY = env.str('API_KEY')
JWT_ISSUER = env.str('JWT_ISSUER')
JWT_AUTHORIZED_PARTY = env.str('JWT_AUTHORIZED_PARTY')
JWT_AUDIENCE = env.str('JWT_AUDIENCE')