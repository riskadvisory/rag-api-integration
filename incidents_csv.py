import csv
import logging

import requests

from settings import API_KEY
from settings import RAG_HOST

logger = logging.getLogger(__name__)


def get_incidents_csv():
    url = f'{RAG_HOST}/v1/incidents'
    params = {
        'key': API_KEY,
        'from_date': '2018-04-15',
        'to_date': '2018-04-20'
    }
    headers = {'Content-Type': 'text/csv'}
    with requests.get(url, params=params, headers=headers, stream=True) as r:
        lines = (line.decode('utf-8') for line in r.iter_lines())
        for row in csv.reader(lines):
            print(row)


if __name__ == '__main__':
    get_incidents_csv()
