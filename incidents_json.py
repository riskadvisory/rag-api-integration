import logging
from pprint import pprint

import requests

from settings import API_KEY
from settings import RAG_HOST

logger = logging.getLogger(__name__)


def get_incidents_json():
    url = f'{RAG_HOST}/v1/incidents?limit=2'
    params = {
        'key': API_KEY,
        'from_date': '2018-04-15',
        'to_date': '2018-04-20'
    }
    print(f'Making call to {url} with parmas {params}')
    r = requests.get(url, params=params)
    print(f'Response status code {r.status_code}')
    response_json = r.json()
    if r.status_code > 399:
        print(f'Got error back {response_json}')
    else:
        print(f'Response metadata {response_json["metadata"]}')
        print(f'Response data {response_json["data"]}')


if __name__ == '__main__':
    get_incidents_json()
