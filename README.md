# Api Integration

- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installing](#installing)
- [Making first API call](#api-call)

## Getting Started

These instructions will help you integrate with RAG APIs.

### Prerequisites

What things you need to install the software and how to install them

```
Python 3.7 or greater
Virtualenv
```

### Installing

Cloning the repo Make virtualenv and install requirements

```shell
pip install -r pip install -r requirements.txt
```
Update .env file with server url and API key

Copy env file and populate host and api key.
```shell
cp .env.example .env
```

### Making first API call

After installing requirements and updating env file use incidents.py to call incidents API. Rest of the apis follow
same direction. Swagger of APIs can be found on the host as well.
```shell
python incidents_json.py
```
All APIs also support csv streaming of data as well.
```shell
python incidents_csv.py
```
To check if you have implemented auth correctly. Please replace PEM_BYTES in the python file with your private key which
should be in PEM format. And provide us with the public part of the key.
```shell
python check_jwt_token.py
```